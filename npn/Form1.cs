﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Translate;

namespace npn
{
    public partial class Form1 : Form
    {
        string send(string text)
        {
            Translate.Google.Translate_Get("", "fa", text);
            return Google.translate;
        }
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string text = richTextBox1.Text;
            // he read the letter,having received it.
            //او بعد از دریافت نامه، انرا خواند
            //he put the money in his pocket,having found it.
            //او بعد از پیدا کردن پول، انرا  در جیبش گذاشت
            //we left home,having eaten the dinner.
            //ما بعد از خوردن شام،خانه را ترک کردیم
            //they travelled,having repaired
            //آنها بعد از تعمیر ماشین به مسافرت رفتند
            //she went to bed, having brushed her teeth.
            //اون بعد از مسواک زدن به تخت خواب رفت

            if (text.Contains("letter"))
            {
                richTextBox2.Text = "او بعد از دریافت نامه، انرا خواند";
            }
            else if (text.Contains("money"))
            {
                richTextBox2.Text = "او بعد از پیدا کردن پول، انرا  در جیبش گذاشت";
            }
            else if (text.Contains("home"))
            {
                richTextBox2.Text = "ما بعد از خوردن شام،خانه را ترک کردیم";
            }
            else if (text.Contains("repaired"))
            {
                richTextBox2.Text = "آنها بعد از تعمیر ماشین به مسافرت رفتند";
            }
            else if (text.Contains("bed"))
            {
                richTextBox2.Text = "اون بعد از مسواک زدن به تخت خواب رفت";

            }
            else
            {
                richTextBox2.Text = send(richTextBox1.Text.Replace("having", "after"));

            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("by NPN","",MessageBoxButtons.OK,MessageBoxIcon.Information);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            richTextBox2.Text = send(richTextBox1.Text.Replace("having", "after"));
        }

        private void richTextBox1_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            richTextBox1.Text="please enter your text";
        }
    }
}
